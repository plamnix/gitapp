
import os
from md5_encoding import md5
import time
import json

def timer(func_to_decorate):
    def wrappered_function():
        start_time = time.time()
        func_to_decorate()
        end_time = time.time()
        print("Checked for {:6.3f}sec".format(end_time - start_time))
    return wrappered_function

def build_encoded_tree(path):
    if os.path.isdir(path):
        tree = {}
        tree[os.path.basename(path)] = [build_encoded_tree(os.path.join(path,x)) for x in os.listdir(path)]
    else:
        tree = {os.path.basename(path) : md5(path)}
    return tree

@timer
def main():
    path =os.getcwd()
    print (path)
    print (json.dumps(build_encoded_tree(path), indent = 3))

    file = open("encodedfiles.json", "w+")
    with file as f:
        f.write(json.dumps(build_encoded_tree(path),indent = 3))
        f.close()

main()